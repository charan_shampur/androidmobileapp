package com.example.charanshampur.zillowsearch;

import android.app.Activity;
import java.io.ByteArrayOutputStream;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import java.security.Signature;
import java.util.Iterator;


public class main extends Activity {


    //String feedUrl = "http://cs-server.usc.edu:12046/test.php";
    String feedUrl;
    TextView errorLab;
    Spinner spinState;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Code for populating the spinner item starts
        spinState=(Spinner) findViewById(R.id.state_spin);
        errorLab= (TextView) findViewById(R.id.errorLab);
// Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.state_array, android.R.layout.simple_spinner_item);
// Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
// Apply the adapter to the spinner
        spinState.setAdapter(adapter);
        // Code for populating the spinner item ends

        PackageInfo info;
        try {
            info = getPackageManager().getPackageInfo("com.example.charanshampur.zillowsearch", PackageManager.GET_SIGNATURES);
            for (android.content.pm.Signature signature : info.signatures) {
                MessageDigest md;
                md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String something = new String(Base64.encode(md.digest(), 0));
                //String something = new String(Base64.encodeBytes(md.digest()));
                Log.i("hash key", something);
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Log.i("name not found", e1.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.i("no such an algorithm", e.toString());
        } catch (Exception e) {
            Log.i("exception", e.toString());
        }


        // code for populating subscript after the label names starts
        TextView addrLab = (TextView)findViewById(R.id.addrLab);
        addrLab.setText(Html.fromHtml("Address<sup>*</sup> :"));

        TextView cityLab = (TextView)findViewById(R.id.cityLab);
        cityLab.setText(Html.fromHtml("City<sup>*</sup> :"));

        TextView stateLab = (TextView)findViewById(R.id.stateLab);
        stateLab.setText(Html.fromHtml("State<sup>*</sup> :"));
        // code for populating subscript after the label names starts


        //code for validating the input fields start

        final EditText street = (EditText) findViewById(R.id.street);
        final EditText city=(EditText) findViewById(R.id.city);
        //final Spinner state= (Spinner) findViewById(R.id.state_spin);
        Button search=(Button) findViewById(R.id.search);
        final TextView streetErr = (TextView) findViewById(R.id.streetErr);
        final TextView cityErr = (TextView) findViewById(R.id.cityErr);
        final TextView stateErr = (TextView) findViewById(R.id.stateErr);
        ImageView zillImage = (ImageView) findViewById(R.id.zillImage);
        //zillowGet zillowObj = new zillowGet();
        //zillowObj.execute();

        zillImage.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                       Uri.parse("http://www.zillow.com/"));
               startActivity(browserIntent);

           }
        });



        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(street.getText().toString().length()==0)
                {
                    streetErr.setVisibility(View.VISIBLE);
                }

                if(city.getText().toString().length()==0)
                {
                    cityErr.setVisibility(View.VISIBLE);
                }

                if(spinState.getSelectedItem().toString().equals("[Choose State]"))
                {
                    stateErr.setVisibility(View.VISIBLE);
                    //Toast.makeText(getApplicationContext(),
                      //      "State is a required field", Toast.LENGTH_LONG).show();
                }

                spinState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i,
                                               long l) {
                     TextView myText = (TextView) view;
                     if (myText.getText().toString().equals("[Choose State]"))
                     {
                         stateErr.setVisibility(View.VISIBLE);
                         //Toast.makeText(getApplicationContext(),
                           //      "State is a required field", Toast.LENGTH_LONG).show();
                     }
                     else
                     {
                         stateErr.setVisibility(View.INVISIBLE);
                     }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });

                city.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        if(city.getText().toString().length()==0)
                        {
                            cityErr.setVisibility(View.VISIBLE);
                        }
                        else
                        {
                            cityErr.setVisibility(View.INVISIBLE);
                        }
                    }
                });

                street.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {

                        if(street.getText().toString().length()==0)
                        {
                            streetErr.setVisibility(View.VISIBLE);
                        }
                        else
                        {
                            streetErr.setVisibility(View.INVISIBLE);
                        }


                    }
                });


                if((street.getText().toString().length()!=0)&&
                   (city.getText().toString().length()!=0)&&
                   (spinState.getSelectedItem().toString().length()!=0))
                {
                    //sample = http://cs-server.usc.edu:12046/
                    // index.php?street=2114+Bigelow+Ave&city=Seattle&state=WA&submit=search
                    String streetName,cityName,stateName;
                    streetName=street.getText().toString();
                    cityName=city.getText().toString();
                    stateName=spinState.getSelectedItem().toString();
                    feedUrl="http://cs-server.usc.edu:12046/index.php?";
                    try {
                        feedUrl+="street="+ URLEncoder.encode(streetName, "UTF-8")+"&city="+URLEncoder.encode(cityName, "UTF-8")+"&state="+URLEncoder.encode(stateName, "UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    zillowGet zillowObj = new zillowGet();
                    zillowObj.execute();
                }
            }
        });
        //code of validating the input fields end


        /*
        background= (LinearLayout) findViewById(R.id.background);
        btnBlue= (Button) findViewById(R.id.btnBlue);
        btnGreen= (Button) findViewById(R.id.btnGreen);

         btnBlue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                background.setBackgroundColor(Color.parseColor("#006699"));
            }
        });

        btnGreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                background.setBackgroundColor(Color.parseColor("#00ff00"));
            }
        }); */

    }


    public class zillowGet  extends AsyncTask<Void, Void, Void>{
        //ProgressDialog dialog;
        String jsonData;
        @Override
        protected void onPreExecute()
        {
           // dialog = new ProgressDialog(context);
            //dialog.setTitle("Loading Videos");
            //dialog.show();
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params)
        {
            HttpClient client = new DefaultHttpClient();
            HttpGet getRequest = new HttpGet(feedUrl);
            try
            {
                HttpResponse response = client.execute(getRequest);
                StatusLine statusLine = response.getStatusLine();
                int statusCode = statusLine.getStatusCode();
                if(statusCode != 200)
                {
                    return null;
                }
                InputStream jsonStream = response.getEntity().getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(jsonStream));
                StringBuilder builder = new StringBuilder();
                String line;
                while((line = reader.readLine())!=null)
                {
                    builder.append(line);
                }
                jsonData = builder.toString();
                Log.i("jsonData",jsonData);
                if(!(jsonData.indexOf("error")>0))
                {
                    Log.i("jsonData",jsonData);
                    //errorLab.setVisibility(View.INVISIBLE);
                    try {
                        JSONObject jsonStr = new JSONObject(jsonData);
                        GlobalDataStore.JsonObj=jsonStr;
                        Log.i("year5",jsonStr.getString("year5"));

                        //JSONObject inputJSON = new JSONObject("{\"Preferences\":{\"Pref1\":\"Apple\", \"Pref2\":\"Pear\"}}");
                        //JSONObject preferencesJSON = inputJSON.getJSONObject("Preferences");

                        Iterator<String> keysIterator = jsonStr.keys();
                        while (keysIterator.hasNext())
                        {
                            String keyStr=keysIterator.next().toString();
                            String valueStr=jsonStr.getString(keyStr).toString();
                            Log.i(keyStr,valueStr);
                        }

                        //Intent intent=new Intent(this);
                        startActivity(new Intent(getApplicationContext(),swipeactivity.class));


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }


            }
            catch (ClientProtocolException e)
            {
// TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (IOException e)
            {
// TODO Auto-generated catch block
                e.printStackTrace();
            }
         /*   catch (JSONException e)
            {
// TODO Auto-generated catch block
                e.printStackTrace();
            }  */
            return null;
        }


        @Override
        protected void onPostExecute(Void result) {
            //dialog.dismiss();
            //videoAdapter.notifyDataSetChanged();
            //super.onPostExecute(result);
            if((jsonData.indexOf("error")>0))
            {
                errorLab.setVisibility(View.VISIBLE);
            }
            else
            {
                errorLab.setVisibility(View.INVISIBLE);
            }

        }
    }



}

