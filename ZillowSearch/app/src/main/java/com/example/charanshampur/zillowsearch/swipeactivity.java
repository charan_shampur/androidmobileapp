package com.example.charanshampur.zillowsearch;

import android.app.ActionBar;
import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.net.wifi.p2p.WifiP2pManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.Toast;

import com.facebook.android.Facebook;
import com.facebook.widget.FacebookDialog;


public class swipeactivity extends FragmentActivity implements ActionBar.TabListener {

    ActionBar actionBar;
    ViewPager viewPager;
    //ImageView imag1 = (ImageView) findViewById(R.id.fbButton);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_swipeactivity);

        viewPager= (ViewPager) findViewById(R.id.pager);
        viewPager.setAdapter(new Myadapter(getSupportFragmentManager()));
        viewPager.setOnPageChangeListener(
                new ViewPager.OnPageChangeListener() {
                    @Override
                    public void onPageScrolled(int i, float v, int i2) {

                    }

                    @Override
                    public void onPageSelected(int i) {

                        actionBar.setSelectedNavigationItem(i);

                    }

                    @Override
                    public void onPageScrollStateChanged(int i) {

                        if(i==ViewPager.SCROLL_STATE_IDLE)
                        {
                            // Log.d("vivz", "SCROLL_STATE_IDLE");
                        }
                        if(i==ViewPager.SCROLL_STATE_DRAGGING)
                        {
                            //Log.d("vivz", "SCROLL_STATE_DRAGGING");
                        }
                        if(i==ViewPager.SCROLL_STATE_SETTLING)
                        {
                            //Log.d("vivz", "SCROLL_STATE_SETTLING");
                        }


                    }
                });



        actionBar=getActionBar();


        if (actionBar != null) {
            actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        }


        assert actionBar != null;
        ActionBar.Tab tab1=actionBar.newTab();
        tab1.setText("BASIC INFO");
        tab1.setTabListener(this);

        ActionBar.Tab tab2=actionBar.newTab();
        tab2.setText("HISTORICAL ZESTIMATES");
        tab2.setTabListener(this);

        // ActionBar.Tab tab3=actionBar.newTab();
        //tab3.setText("Tab 3");
        //tab3.setTabListener(this);

        actionBar.addTab(tab1);
        actionBar.addTab(tab2);
        //   actionBar.addTab(tab3);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_swipeactivity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        viewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {

    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        FragmentA.uiHelper.onActivityResult(requestCode, resultCode, data, dialogCallback);
        Log.i("Function","onActivityResult");
    }

    private FacebookDialog.Callback dialogCallback = new FacebookDialog.Callback() {
        @Override
        public void onComplete(FacebookDialog.PendingCall pendingCall, Bundle data) {
            Log.i("swipeactivity", "oncomplete");
            String postId = FacebookDialog.getNativeDialogPostId(data);
            //Log.i("post-id",postId);
            Toast.makeText(getApplicationContext(),
                    "Post Successfull", Toast.LENGTH_LONG).show();
        }

        @Override
        public void onError(FacebookDialog.PendingCall pendingCall, Exception error, Bundle data) {
            //Log.d(TAG_LOG, String.format("Error: %s", error.toString()));
            Log.i("Function", "onError");
            Log.i("swipeactivity", "onerror");
        }
    };

}

class Myadapter extends FragmentPagerAdapter
{

    public Myadapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int i) {
        Fragment fragment=null;

        if(i==0)
            fragment = new FragmentA();
        if(i==1)
            fragment = new FragmentB();

        return fragment;
    }

    @Override
    public int getCount() {
        return 2;
    }
}
