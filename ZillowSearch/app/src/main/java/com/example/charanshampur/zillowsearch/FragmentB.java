package com.example.charanshampur.zillowsearch;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
//import android.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.app.Fragment;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AbsoluteLayout;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ViewSwitcher;


import org.json.JSONException;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentB extends Fragment implements ViewSwitcher.ViewFactory {
    int count = 0;
    int numDrawables = 3;
    Drawable[] drawableArray = new Drawable[numDrawables];
    ImageSwitcher is;
    String[] year = new String[3];
    TextView zestYear;

    public FragmentB() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_b, container, false);

        is = (ImageSwitcher) view.findViewById(R.id.chartSwitcher);
        Button prev = (Button) view.findViewById(R.id.prev);
        Button next = (Button) view.findViewById(R.id.next);
        is.setFactory(this);
        //is.setInAnimation(this,android.R.anim.slide_in_left);
        //is.setOutAnimation(this,android.R.anim.slide_out_right);

        //Drawable image = ImageOperations(this, path, "image.jpg");

        //ImageView imageView = (ImageView) view.findViewById(R.id.image);
        //imageView.setImageDrawable(image);
        DownloadImageTask Imagetask = new DownloadImageTask();
        Imagetask.execute();
        zestYear = (TextView) view.findViewById(R.id.textDuration);
        TextView zestAddrStr = (TextView) view.findViewById(R.id.LocationAddr);

        String Address = null;
        try {
            Address = GlobalDataStore.JsonObj.getString("street") + ", "
                    + GlobalDataStore.JsonObj.getString("city") + ", "
                    + GlobalDataStore.JsonObj.getString("state") + "-"
                    + GlobalDataStore.JsonObj.getString("zipcode");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        zestAddrStr.setText(Address);

        year[0]="Historical Zestimates for the past 1 year";
        year[1]="Historical Zestimates for the past 5 year";
        year[2]="Historical Zestimates for the past 10 year";



        count = 0;
        is.setInAnimation(getActivity(), android.R.anim.fade_in);
        is.setOutAnimation(getActivity(), android.R.anim.fade_out);
        //Animation in = AnimationUtils.loadAnimation(getActivity(),android.R.anim.slide_in_left);


            is.setImageDrawable(drawableArray[0]);
            zestYear.setText(year[0]);

        TextView disc3 = (TextView) view.findViewById(R.id.Disc3);
        TextView disc4 = (TextView) view.findViewById(R.id.Disc4);

        disc3.setPaintFlags(disc3.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        disc4.setPaintFlags(disc4.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        disc3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("http://www.zillow.com/corp/Terms.htm"));
                startActivity(browserIntent);

            }
        });

        disc4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("http://www.zillow.com/zestimate/"));
                startActivity(browserIntent);

            }
        });




        prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (count > 0) {
                    count--;
                    try {
                        is.setImageDrawable(drawableArray[count]);
                        zestYear.setText(year[count]);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    count = 2;
                    try {
                        is.setImageDrawable(drawableArray[count]);
                        zestYear.setText(year[count]);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (count < 2) {
                    try {
                        count++;
                        is.setImageDrawable(drawableArray[count]);
                        zestYear.setText(year[count]);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    count = 0;
                    try {
                        is.setImageDrawable(drawableArray[count]);
                        zestYear.setText(year[count]);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }
        });

        return view;
    }


    @Override
    public View makeView() {

        ImageView iv = new ImageView(getActivity());
        iv.setScaleType(ImageView.ScaleType.FIT_XY);
        //iv.setScaleType(ImageView.ScaleType.FIT_CENTER);
        iv.setLayoutParams(new ImageSwitcher.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT));

        return iv;
    }

    public void renderView()
    {
        is.setImageDrawable(drawableArray[0]);
        zestYear.setText(year[0]);
    }

    public class DownloadImageTask extends AsyncTask<Object,Void,Void> {

        @Override
        protected Void doInBackground(Object[] objects) {

            URL url = null;
            try {
                url = new URL(GlobalDataStore.JsonObj.getString("year1"));
                InputStream inputFeed1 = (InputStream) url.getContent();
                Drawable draw1 = Drawable.createFromStream(inputFeed1, "src");
                url = new URL(GlobalDataStore.JsonObj.getString("year5"));
                InputStream inputFeed2 = (InputStream) url.getContent();
                Drawable draw2 = Drawable.createFromStream(inputFeed2, "src");
                url = new URL(GlobalDataStore.JsonObj.getString("year10"));
                InputStream inputFeed3 = (InputStream) url.getContent();
                Drawable draw3 = Drawable.createFromStream(inputFeed3, "src");
                drawableArray[0] = draw1;
                drawableArray[1] = draw2;
                drawableArray[2] = draw3;
        //        GlobalDataStore.drawableArray1=drawableArray;


            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void Result) {
            //dialog.dismiss();
            //videoAdapter.notifyDataSetChanged();
            is.setImageDrawable(drawableArray[0]);
            zestYear.setText(year[0]);

        }
    }
}
